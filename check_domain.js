#!/usr/bin/env node

'use strict'
import whois from 'whoiser';

let days;
const servers = ['whois.verisign-grs.com', 'whois.registry.tcinet.ru', 'whois.reg.ru', 'whois.crsnic.net', 'whois.tcinet.ru', 'whois.nic.ru', 'whois.nic.tv'];
const domain = process.argv[2];
const expiryDateKey = 'Expiry Date';

/* Можно и так */
/* let domain = process.argv.slice(2);
domain.forEach(element => {
	domain = element;
}); */

const checkDomain = async (url, servers) => {
	return new Promise(async (resolve, reject) => {
		let domainInfo;
		let serverWhois;

		try {
			for (let server of servers) {
				if (!server) throw new Error('в массиве серверов!');
				serverWhois = server;
				domainInfo = await whois(url, { host: server });
				/* console.log(domainInfo) */
				if (domainInfo[server][expiryDateKey]) {
					break;
				} else {
					continue;
				}
			}
			if (!domainInfo[serverWhois][expiryDateKey]) {
				throw new Error('Нет подходящего сервера Whois')
			}
		} catch (error) {
			reject(error);
		}

		resolve({
			server: serverWhois,
			domain: domainInfo
		});
	});
}

checkDomain(domain, servers)
	.then(data => {
		let serverWhois = data.server;
		let expiryDate = data.domain[serverWhois][expiryDateKey];

		/* console.log(data) */

		if (!expiryDate) console.log(expiryDate);
		if (expiryDate.length > 10) expiryDate = expiryDate.slice(0, expiryDate.length - 10);

		let deadline = getTimeRemaining(expiryDate);
		console.log(deadline)
	})
	/* Тоже рабочий вариант. Проверка по имени Ошибки  Проброс события ошибка*/
	/* .catch(error => {
		if (error.name != 'TypeError') {
			throw error;
		}
		else {
			console.log(error.name)
			console.log(`${error.message} Проверить сервера в скрипте!`)
		}
	}) */
	.catch(error => {
		console.log(`Ошибка ${error.message}`)
	})


function getTimeRemaining(endtime) {
	const t = Date.parse(endtime) - Date.parse(new Date());
	if (t <= 0) {
		days = 0
	} else {
		days = Math.floor((t / (1000 * 60 * 60 * 24)))
	}
	return days
}