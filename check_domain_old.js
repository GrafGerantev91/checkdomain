#!/usr/bin/env node

'use strict'
import whois from 'whoiser';

let days;
const servers = ['whois.verisign-grs.com', 'whois.registry.tcinet.ru', /* 'whois.nic.ru', */ 'whois.crsnic.net'];
const domain = process.argv[2];
const expiryDateKey = 'Expiry Date';

/* Можно и так */
/* let domain = process.argv.slice(2);
domain.forEach(element => {
	domain = element;
}); */

const checkDomain = async (url, servers) => {
	let domainInfo;
	let serverWhois;
	for (let server of servers) {
		serverWhois = server;
		domainInfo = await whois(url, {
			host: server
		})
		if (domainInfo[server][expiryDateKey]) {
			break;
		} else {
			continue;
		}
	}
	return {
		server: serverWhois,
		domain: domainInfo
	};
}


checkDomain(domain, servers)
	.then(data => {
		let serverWhois = data.server;
		let expiryDate = data.domain[serverWhois][expiryDateKey];
		/* console.log(data) */
		expiryDate = expiryDate.slice(0, expiryDate.length - 10);
		let deadline = getTimeRemaining(expiryDate);
		/* console.log(deadline) */
	})
	.catch(console.log(1))

function getTimeRemaining(endtime) {
	const t = Date.parse(endtime) - Date.parse(new Date());
	if (t <= 0) {
		days = 0
	} else {
		days = Math.floor((t / (1000 * 60 * 60 * 24)))
	}
	return days
}