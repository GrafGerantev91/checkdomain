#!/usr/bin/env node

import sslChecker from "ssl-checker";
const domain = process.argv[2];

const getSslDetails = async (domain) => {
	return new Promise(async (resolve, reject) => {
		let sslCheck;
		try {
			sslCheck = await sslChecker(domain, { method: "GET", port: 443 });

		} catch (error) {
			reject(error);
		}
		resolve(sslCheck.daysRemaining)
	})
}

getSslDetails(domain)
	.then(data => {
		console.log(data)
	})

